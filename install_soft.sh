#!/bin/bash
##########
#系统工具
##########
#update
apt-get --force-yes -y  update
#unrar
apt-get --force-yes -y  install rar unrar p7zip-rar p7zip-full cabextract
ln -fs /usr/bin/rar /usr/bin/unrar

#linux+apache+php+mysql
apt-get --force-yes -y install php5-cli
apt-get --force-yes -y install apache2 php5-mysql libapache2-mod-php5 mysql-server

#yii2
curl -s http://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
apt-get install nginx

#右键加入终端
apt-get --force-yes -y  install nautilus-open-terminal

#右键管理员方式打开
apt-get --force-yes -y  install nautilus-gksu

#ubuntu使用RPM包 
apt-get --force-yes -y  install alien

#显示CPU温度命令
#apt-get --force-yes -y  install lm-sensors


##########
# 开发工具
##########
#基础编译工具
apt-get --force-yes -y install build-essential
apt-get --force-yes -y install autoconf automake libtool

#应用开发
#apt-get --force-yes -y install valgrind

#内核开发
#apt-get --force-yes -y install kernel-package libncurses5-dev libqt3-headers libglade2-dev

#man 手册
apt-get --force-yes -y  install manpages manpages-dev manpages-posix manpages-posix-dev manpages-de manpages-de-dev binutils-doc cpp-doc gcc-doc glibc-doc 

#minicom
#apt-get --force-yes -y  install minicom
#echo "alias minicom='env LANG=en_US minicom'" >> ~/.bashrc

#openocd
#apt-get --force-yes -y  install openocd

#qt4
#apt-get --force-yes -y install zlib1g-dev 
#apt-get --force-yes -y install qtcreator

#sdl库
#apt-get --force-yes -y install libsdl1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsdl-gfx1.2-dev

##########
#文本处理
##########
#vim
apt-get --force-yes -y install vim vim-gnome exuberant-ctags vim-doc vim-gui-common cscope

#xmlto
#apt-get --force-yes -y install xmlto

#chmsee
#apt-get --force-yes -y install chmsee 

#stardic
#apt-get --force-yes -y install stardict
#tar xzf dic.tar.gz -C /usr/share/stardict

#pdf
#dpkg -i AdbeRdr9.3.3-1_i386linux_enu.deb
#dpkg -i FoxitReader_1.1.0_i386.deb
#apt-get --force-yes -y install poppler-data cmap-adobe-cns1 cmap-adobe-gb1 
#mv /etc/fonts/conf.d/49-sansserif.conf /etc/fonts/conf.d/49-sansserif.conf.bak


##########
#网络工具
##########
#NFS服务器
#apt-get --force-yes -y  install nfs-kernel-server nfs-client nfs-common portmap

#ssh
apt-get --force-yes -y  install openssh-server openssh-client

#安装tftp服务器
#apt-get --force-yes -y  install xinetd tftpd-hpa tftp-hpa

#网络服务
#apt-get --force-yes -y install sysv-rc-conf
#apt-get --force-yes -y install chkconfig

#软件仓库
apt-get --force-yes -y install git
#apt-get --force-yes -y install cvs
#apt-get --force-yes -y install subversion

#ftp客户端
#apt-get --force-yes -y install gftp 

#网络抓包
#apt-get --force-yes -y install wireshark 

##########
#多媒体
##########
#解决播放器列表显示乱码工具
#apt-get --force-yes -y install python-mutagen
#apt-get --force-yes -y install audacious audacious-plugins
#apt-get --force-yes -y install mplayer mplayer-fonts gnome-mplayer


########
# 虚拟化
########
#apt-get --force-yes -y install qemu-kvm
#apt-get --force-yes -y install wine
#apt-get --force-yes -y install virtualbox

#apt-get --force-yes -y upgrade && apt-get --force-yes -y dist-upgrade 

